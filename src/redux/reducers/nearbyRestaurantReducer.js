import * as ActionTypes from '../ActionTypes';

const initialState = {
    restaurants: { },
    isFetching: false,
    error: false
}

/** Fetch Nearby Restaurant Reducer */
export default nearbyRestaurantReducer = (state = initialState, action ) => {
    switch(action.type) {
        case ActionTypes.NEARBY_RESTAURANTS:
            return {
                ...state,
                isFetching: true
            }   

        case ActionTypes.NEARBY_RESTAURANTS_SUCCESS:
        console.log("Reducers")
        console.log(action.payload);
             return {
                 ...state,
                 isFetching: false,
                 restaurants: action.payload
             }    

        case ActionTypes.NEARBY_RESTAURANTS_FAILURE: 
            return {
                ...state,
                isFetching: false,
                error: true        
            }     

         default :
            return state;   
    }
}