import * as ActionTypes from '../ActionTypes';

const initialState = {
    reviews: { },
    isFetching: false,
    error: false
}

/**Fetch user Reviews Reducer */
export default fetchUserReviewReducer = (state = initialState, action ) => {
    switch(action.type) {
        case ActionTypes.FETCH_USER_REVIEWS_SUCCESS:
        console.log("Reducers")
         console.log(action.payload);
             return {
                 ...state,
                 isFetching: false,
                 reviews: action.payload
             }    

        case ActionTypes.FETCH_USER_REVIEWS_FAILURE: 
            return {
                ...state,
                isFetching: false,
                error: true        
            }     

         default :
            return state;   
    }
}