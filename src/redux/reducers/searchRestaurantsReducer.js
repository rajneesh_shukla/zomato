import * as ActionTypes from '../ActionTypes';

const initialState = {
    restaurants: { },
    isFetching: false,
    error: false
}

/** Search Restaurant Reducer */
export default searchRestaurantsReducer = (state = initialState, action ) => {
    switch(action.type) {
        case ActionTypes.SEARCH_RESTAURANTS_SUCCESS:
        console.log("Reducers")
         console.log(action.payload);
             return {
                 ...state,
                 isFetching: false,
                 restaurants: action.payload
             }    

        case ActionTypes.SEARCH_RESTAURANTS_FAIL: 
            return {
                ...state,
                isFetching: false,
                error: true        
            }     

         default :
            return state;   
    }
}