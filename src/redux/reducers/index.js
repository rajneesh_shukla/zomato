import { combineReducers } from 'redux';
import nearbyRestaurantReducer from './nearbyRestaurantReducer';
import searchRestaurantsReducer from './searchRestaurantsReducer';
import fetchUserReviewReducer from './userReviewReducer';

// Combine Reducers
const rootReducer = combineReducers({
    nearbyRestaurantReducer,
    searchRestaurantsReducer,
    fetchUserReviewReducer
})

export default rootReducer;