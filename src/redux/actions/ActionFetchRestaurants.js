import * as actionTypes from '../ActionTypes';
import { baseUrl, user_key } from '../../constants/constants';

/**
 *  Fetch Neadrby Restaurants 
 *  Receive current location lat lng
 * @param {* double } lat 
 * @param {* double} lng 
 */
export const fetchNearbyRestaurants = (lat, lng) => (dispatch) => {
     
     return  fetch(baseUrl + '/geocode?lat='+ lat +'&lon='+ lng +'', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'user-key': user_key
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
               return (dispatch(nearbyRestaurantsSuccess(responseJson)))
            }
            )
            .catch( err => dispatch(nearbyRestaurantsFailure(err)));
}

function getRestaurants(){
    return { type : actionTypes.NEARBY_RESTAURANTS }
}

function nearbyRestaurantsSuccess(data) {
    return {
        type: actionTypes.NEARBY_RESTAURANTS_SUCCESS,
        payload: data
    }
}

function nearbyRestaurantsFailure() {
    return {
        type: actionTypes.NEARBY_RESTAURANTS_FAILURE
    }
}
