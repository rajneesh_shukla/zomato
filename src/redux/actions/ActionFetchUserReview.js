import * as actionTypes from '../ActionTypes';
import { user_key, baseUrl } from '../../constants/constants';

/**
 * Fetch user Reviews based on restaurant Id
 * @param {number } res_id 
 */
export const fetchUsersReviews = (res_id) => (dispatch) => {

        console.log("res id = "+ res_id);
        const reviewApiUrl = ''+ baseUrl +'reviews?res_id=' + res_id + '&start=1&count=5';
    
        return  fetch( reviewApiUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'user-key': user_key
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
               return (dispatch(fetchUsersReviewSuccess(responseJson)))
            }
            )
            .catch( err => dispatch(fetchUsersReviewFailure(err)));
}


function fetchUsersReviewSuccess(data) {
    return {
        type: actionTypes.FETCH_USER_REVIEWS_SUCCESS,
        payload: data
    }
}

function fetchUsersReviewFailure() {
    return {
        type: actionTypes.FETCH_USER_REVIEWS_FAILURE
    }
}
