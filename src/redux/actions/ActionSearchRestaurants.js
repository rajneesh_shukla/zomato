import * as actionTypes from '../ActionTypes';
import { baseUrl, user_key } from '../../constants/constants';

/**
 * Search Restaurant details based on search quary and following parameters
 * @param {*integer} entity_id 
 * @param {* integer} entity_type 
 * @param {* string} q 
 * @param {* integer} start 
 * @param {* integer} count 
 * @param {* double} lat 
 * @param {* double} long 
 * @param {* integer} radius 
 */
export const searchRestaurants = (entity_id, entity_type, q, start, count, lat, long, radius) => (dispatch) => {
   
    const SearchUrl = baseUrl+ 'search?entity_id='+ entity_id +'&entity_type='+ entity_type +
                       '&q='+ q +'&start='+ start +'&count='+ count +
                       '&lat='+ lat +'&lon='+ long +'&radius='+ radius +'&order=asc';

    return  fetch(SearchUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'user-key': user_key
        }
    })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            return (dispatch(searchRestaurantsSuccess(responseJson)))
        }
        )
        .catch(err => dispatch(searchRestaurantsFailure(err)));
}


function searchRestaurantsSuccess(data) {
    return {
        type: actionTypes.SEARCH_RESTAURANTS_SUCCESS,
        payload: data
    }
}

function searchRestaurantsFailure() {
    return {
        type: actionTypes.SEARCH_RESTAURANTS_FAIL
    }
}
