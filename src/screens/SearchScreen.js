import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Icon } from 'native-base';
import RestaurantList from '../Cards/RestaurantList';
import { connect } from 'react-redux';

import { searchRestaurants } from '../redux/actions/ActionSearchRestaurants';

//search the restaurant 
class SearchScreen extends Component {

    static navigationOptions = {
        headerRight: <Icon name="filter" size={24}
            style={{ marginRight: 10 }}
            type='FontAwesome'
            onPress={() => navigation.toggleDrawer()} />
    };

    constructor(props) {
        super(props);
        this.state = {
            search: false,
            restaurants: null,
            searchQuery: "tea"
        }
    }

    componentWillReceiveProps(nextProp) {
        // Update the state when data  is fetched
        if (nextProp.SearchedRestaurantsData.isFetching == false) {
            console.log(nextProp.SearchedRestaurantsData.restaurants.restaurants);
            this.setState({
                restaurants: nextProp.SearchedRestaurantsData.restaurants.restaurants
            })

            console.log(this.state)
        }
    }


    // Search the nearby restaurants and dishhes
    searchNearby(text) {
        this.setState({
            search: true,
            searchQuery: text
        })
        console.log(this.state)

        // Receive data send from location component
        const restaurantData = this.props.navigation.getParam('restaurantData');
        console.log(restaurantData)

        // search quesry parameter
        const entity_id = restaurantData.location.entity_id;
        const entity_type = restaurantData.location.entity_type;
        const q = text;
        const start = 1;
        const count = 20;
        const lat = restaurantData.location.latitude;
        const long = restaurantData.location.longitude;
        const radius = 2000;
        console.log(entity_id, entity_type, q, start, count, lat, long, radius);

        this.props.searchRestaurants(entity_id, entity_type, q, start, count, lat, long, radius);
    }

    // Show current location component
    showSearchOption() {
        if (this.state.search == false) {
            return (
                <View style={styles.locationContainer} >
                    <Image source={require('../assests/location_search.png')} style={styles.localityIcon} />
                    <Text style={styles.locationText}
                        onPress={() => alert('ffg')}
                    >
                        Use current location
                    </Text>
                </View>
            )
        } else {
            return (
                <FlatList
                    data={this.state.restaurants}
                    renderItem={({ item }) => this.displaySearchResult(item)}
                    keyExtractor={item => item.restaurant.id.toString()}
                />
            )
        }
    }

    /**
     * Display the list of Restaurants
     */
    displaySearchResult(item) {
        return (
            <TouchableOpacity onPress={() => this.showRestaurantDetail(item)}>
                <RestaurantList
                    imageUri={item.restaurant.thumb}
                    name={item.restaurant.name}
                    rating={item.restaurant.user_rating.aggregate_rating}
                    locality={item.restaurant.location.locality}
                    cuisines={item.restaurant.cuisines}
                />
            </TouchableOpacity>
        )
    }


    // Open Restaurant Detail screen
    showRestaurantDetail(item) {
        this.props.navigation.navigate('RestaurantDetail', { restaurant: item });
    }

    /** Set the state to false when all text is cleared in SearchBar */
    onClearSearchText() {
        this.setState({
            search: false
        })
        this.showSearchOption();
        console.log("completed")
    }


    render() {
        return (
            <View>
                <View style={styles.container}>
                    <SearchBar
                        containerStyle={{ backgroundColor: 'white' }}
                        round={true}
                        style={styles.searchBarStyle}
                        lightTheme
                        icon={{ type: 'font-awesome', name: 'search' }}
                        placeholder='Search forrestaurant, cuisine or dish...'
                        onChangeText={(text) => this.searchNearby(text)}
                        onClearText={() => this.onClearSearchText()}
                    />

                    <Text style={styles.searchHint}>
                        E. G. Connaught Place, New Delhi
                    </Text>
                </View>

                <View>
                    {this.showSearchOption()}
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("Search Screen Called");
    return {
        SearchedRestaurantsData: state.searchRestaurantsReducer
    }
}

const styles = {
    container: {
        backgroundColor: 'white'
    },
    searchHint: {
        paddingLeft: 10,
        paddingTop: 0,
        fontSize: 13
    },
    locationContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        width: '100%',
        alignItems: 'flex-start',
        padding: 20,
        paddingTop: 20,
        height: 60,
        borderBottomColor: 'black',
        borderTopColor: 'black'
    },
    localityIcon: {
        alignItems: 'flex-start',
        height: 20, width: 20
    },
    locationText: {
        alignItems: 'flex-start',
        fontSize: 16,
        paddingLeft: 6,
        color: 'green'
    }
}

export default connect(mapStateToProps, { searchRestaurants })(SearchScreen);