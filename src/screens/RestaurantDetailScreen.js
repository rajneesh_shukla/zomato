import React, { Component } from 'react';
import { StyleSheet, View, FlatList, ActivityIndicator } from 'react-native';
import { Card, CardItem, Right, Left, Body, Text, Icon } from 'native-base'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import { connect } from 'react-redux';

import { fetchUsersReviews } from '../redux/actions/ActionFetchUserReview';
import ReviewCard from '../Cards/ReviewCard';

/**
 *  Display Restaurants Details and reviews
 */
class RestaurantDetail extends Component {

    constructor() {
        super();
        this.state = {
            restaurant: null,
            reviews: null
        }
    }

    static navigationOptions = {
        header: null,
        drawerIcon: ({ titntColor }) => (
            <Icon
              name='sign-in'
              type='font-awesome'
              size={24}
              color={titntColor}
            />
          )
    }

    componentWillMount() {
        const restaurantData = this.props.navigation.getParam('restaurant');
        this.setState({
            restaurant: restaurantData.restaurant
        });
        console.log(restaurantData.restaurant.R.res_id)
        this.props.fetchUsersReviews(restaurantData.restaurant.R.res_id);
    }

    componentDidMount() {

    }

    /**
     * Display user reviews
     */
    showUserReview() {
        // if api is fetching show indicator
        if (this.props.Reviews.isFetching != false) {
            return (
                <ActivityIndicator size='small' />
            )
        } else {
            // display the reviews 
            return (
                <FlatList
                    data={this.props.Reviews.reviews.user_reviews}
                    renderItem={({ item }) => <ReviewCard reviewData={item} />}
                    keyExtractor={item => item.review.id.toString()}
                />
            )
        }
    }

    render() {
        // If thumbnail url is empty
        if (this.state.restaurant.thumb == "") {
            this.state.restaurant.thumb = 'https://b.zmtcdn.com/data/pictures/5/55/d62c789369fe159497aeb61a0d330259.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A';
        }
        return (
            <HeaderImageScrollView
                maxHeight={200}
                minHeight={56}
                headerImage={{ uri: this.state.restaurant.thumb }}
            >
                <View style={{ height: 1500 }}>
                    <TriggeringView onHide={() => console.log('text hidden')} >

                        <View>
                            <Card transparent style={styles.titleContainer}>
                                <CardItem style={styles.titleCard}>
                                    <Left style={styles.titleLeft}>
                                        <Body style={styles.titleLeft}>
                                            <Text style={styles.restaurantTitle}>{this.state.restaurant.name}</Text>
                                            <Text>{this.state.restaurant.location.locality_verbose}</Text>
                                        </Body>
                                    </Left>
                                    <Right style={styles.titleRight}>
                                        <Text style={styles.rating}>
                                            {this.state.restaurant.user_rating.aggregate_rating}
                                        </Text>
                                    </Right>
                                </CardItem>

                                <CardItem style={styles.heading}>
                                    <Text style={styles.headingTitle}>ADDRESS</Text>
                                    <Text note style={styles.des_text}>{this.state.restaurant.location.address}</Text>
                                    <Text note style={styles.des_text}>{this.state.restaurant.location.locality_verbose}</Text>
                                </CardItem>

                                <CardItem style={styles.heading}>
                                    <Text style={styles.headingTitle}>CUISINES</Text>
                                    <Text note style={styles.des_text}>{this.state.restaurant.cuisines}</Text>
                                </CardItem>

                                <CardItem style={styles.heading}>
                                    <Text style={styles.headingTitle} note >AVERAGE COST </Text>
                                    <Text note style={styles.des_text}>₹{this.state.restaurant.average_cost_for_two} for two peaple (approx) </Text>
                                </CardItem>
                            </Card>
                        </View>

                        <View>
                           <Text style={styles.reviewsHeading} note >RECENT REVIEWS </Text>                            
                            {this.showUserReview()}
                        </View>

                    </TriggeringView>
                </View>
            </HeaderImageScrollView>
        );
    }
}

const mapStateToProps = state => {
    console.log("Location Component called ")
    return {
        Reviews: state.fetchUserReviewReducer
    }
}

const styles = StyleSheet.create({
    heading: {
         marginLeft: 0,
          marginTop: 0, 
          flexDirection: 'column', 
          alignItems: 'flex-start' 
    },
    container: {
        backgroundColor: '#ffdd00',
        flex: 1
    },
    titleContainer: {
        color: 'black',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    titleLeft: {
        flexDirection: 'column',
        marginLeft: 0,
        flex: 8,
        alignItems: 'flex-start',
        height: 50,
        paddingLeft: 0
    },
    restaurantTitle: {
        fontSize: 20,
        alignContent: 'flex-start',
        fontWeight: 'bold'
    },
    rating: {
        padding: 3,
        color: 'white',
        backgroundColor: '#5BA829'
    },
    titleRight: {
        flex: 2
    },
    titleCard: { 
        marginLeft: 0, 
        marginBottom: 0, 
        alignItems: 'flex-start' 
    },
    headingTitle: { 
        color: 'gray', 
        fontWeight: 'bold' 
    },
    des_text: {
        color: 'black'
    },
    reviewsHeading: { 
        color: 'gray', 
        fontWeight: 'bold', 
        marginLeft: 16 
    }
})

export default connect(mapStateToProps, { fetchUsersReviews })(RestaurantDetail);