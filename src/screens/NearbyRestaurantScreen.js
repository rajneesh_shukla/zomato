import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';
import { Header, Icon, Left, Right } from 'native-base';
import { connect } from 'react-redux';
import MapComponent from '../components/MapComponent';
import RestaurantList from '../Cards/RestaurantList';

/**
 * Display  Restaurant detail in list 
 */
class RestaurantDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mapView: false,
            rightTitle: 'Map View'
        }
    }


    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="wine" style={{ color: tintColor }} />
        ),
        header: null  // Hide default header
    }

    // Will render Restaurant Detail Screen
    showRestaurantDetail(item) {
        this.props.navigation.navigate('RestaurantDetail', { restaurant: item.item });
    }

    // Render the Card of Restaurant of One Restaurant
    renderRestaurantItem(item) {
        console.log(item.index);
        return (
            <TouchableOpacity onPress={() => this.showRestaurantDetail(item)}>
                <RestaurantList
                    imageUri={item.item.restaurant.thumb}
                    name={item.item.restaurant.name}
                    rating={item.item.restaurant.user_rating.aggregate_rating}
                    locality={item.item.restaurant.location.locality}
                    cuisines={item.item.restaurant.cuisines}
                />
            </TouchableOpacity>
        )
    }

    // Will  render the list of restarants
    renderRestaurantList() {
        if (this.props.Restaurants.isFetching == false && this.state.mapView == false) {
            return (
                <FlatList
                    data={this.props.Restaurants.restaurants.nearby_restaurants}
                    renderItem={(item) => this.renderRestaurantItem(item)}
                    keyExtractor={item => item.restaurant.id.toString()}
                />
            )
        } else {
            // Return Map component to display restaurants on map
            return (
                <MapComponent restaurants={this.props.Restaurants.restaurants} />
            )
        }
    }

    // This will toggle the Right text of the header 
    toggleRightHeader() {
        if (this.state.mapView == false) {
            this.setState({
                mapView: true,
                rightTitle: 'List View'
            })
        } else {
            this.setState({
                mapView: false,
                rightTitle: 'Map View'
            })
        }
    }

    // Render the UI
    render() {
        return (
            <View>
                <View>
                    <Header style={styles.header} >
                        <Left>
                            <Icon
                                onPress={() => this.props.navigation.openDrawer()}
                                name="menu" />
                        </Left>
                        <Right>
                            <Text style={{ color: 'green' }}
                                onPress={() => this.toggleRightHeader()} >
                                {this.state.rightTitle}
                            </Text>
                        </Right>
                    </Header>
                </View>
                <View>
                    {this.renderRestaurantList()}
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("Nearby Restaurants Screen ")
    return {
        Restaurants: state.nearbyRestaurantReducer
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rating: {
        backgroundColor: 'green',
        color: 'white',
        padding: 4,
        flex: 1
    },
    cardContainer: {
        marginLeft: 5,
        marginRight: 5
    },
    title: {
        fontSize: 15,
        color: 'black',
        flex: 9
    },
    header: {
        backgroundColor: 'white'
    }

});

export default connect(mapStateToProps)(RestaurantDetailScreen);