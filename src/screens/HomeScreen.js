import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Header, Left, Icon, Body } from 'native-base';
import LocationComponent from '../components/LocationComponent';

/** Home Screen */
export default class HomeScreen extends Component {

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="home" style={{ color: tintColor }} />
        ),
        header: null
    }

    render() {        
        return (
            <View>
                <Header style={styles.header} >
                    <Left>
                        <Icon
                            onPress={() => this.props.navigation.openDrawer()}
                            name="menu" />
                    </Left>
                    <Body>
                        <Text style={styles.headerTitle}>Zomato</Text>
                    </Body>
                </Header>

                <View style={styles.container}>
                    <LocationComponent navigation={this.props.navigation} />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: { 
        backgroundColor: 'white'
    },
    container: {

    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
