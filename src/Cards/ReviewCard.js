import React from 'react';
import { StyleSheet } from 'react-native';
import { Card, CardItem, Right, Left, Body, Button, Thumbnail, Text, Icon } from 'native-base'

/**
 * Display user reviews 
 */
export default ReviewCard = (props) => {

    const comments = props.reviewData.review.comments_count;
    const likes = props.reviewData.review.likes;
    const review_text = props.reviewData.review.review_text;
    const time = props.reviewData.review.review_time_friendly;
    const name = props.reviewData.review.user.name;
    const profile_image = props.reviewData.review.user.profile_image;
    const foodie_level = props.reviewData.review.user.foodie_level;
    const rating = props.reviewData.review.rating;
    
    return (
        <Card>
            <CardItem>
                <Left>
                    <Thumbnail source={{ uri: profile_image }} />
                    <Body>
                        <Text>{name}</Text>
                        <Text note >{foodie_level}</Text>
                    </Body>
                </Left>
                <Right>
                    <Text style={styles.reatingStyle}>{rating}</Text>
                </Right>
            </CardItem>

            <CardItem cardBody>
                <Text note style={styles.reviewText}> {review_text}</Text>
            </CardItem>

            <CardItem>
                <Left>
                    <Button transparent>
                        <Icon active name="thumbs-up" />
                        <Text>{likes} likes</Text>
                    </Button>
                </Left>
                <Body>
                    <Button transparent>
                        <Icon active name="chatbubbles" />
                        <Text>{comments} comment</Text>
                    </Button>
                </Body>
                <Right>
                    <Text>{time}</Text>
                </Right>
            </CardItem>
        </Card>
    );
}

const styles = StyleSheet.create({
    reatingStyle: {
        padding: 3,
        color: 'white',
        backgroundColor: '#5BA829',
        marginTop: 10,
        alignItems: 'flex-end',
        borderRadius: 4,
    },
    reviewText:{ 
        paddingLeft: 10,
        paddingRight: 10,
        color: 'black'
    }
})
