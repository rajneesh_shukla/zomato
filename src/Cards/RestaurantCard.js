import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';

/** 
 * Display squara restaurant card
 */
export class RestaurantCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            restaurants: this.props.restaurantItem  // this is the array of restaurants
        }
    }


    // Will render Restaurant Detail Screen
    showRestaurantDetail(item) {
        this.props.navigation.navigate('RestaurantDetail', { restaurant: item });
    }

    // Render card with detail of restaurant 
    renderRestaurantCard(item) {

        if (item.restaurant.thumb == "") {
            return (
                <TouchableOpacity onPress={() => this.showRestaurantDetail(item)}>
                    <View style={styles.restaurantCard}>
                        <Card
                            key={item.restaurant.R.res_id}
                            image={require('../assests/thumb.jpg')}
                        >
                            <Text style={{ fontWeight: 'bold' }} >{item.restaurant.name}</Text>
                            <Text >
                                The idea cture
                        </Text>
                        </Card>
                    </View>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity onPress={() => this.showRestaurantDetail(item)}>
                    <View style={styles.restaurantCard}>
                        <Card
                            key={item.restaurant.R.res_id}
                            image={require('../assests/thumb.jpg')}
                        >
                            <Text style={{ fontWeight: 'bold' }}>{item.restaurant.name}</Text>
                            <Text >
                                The idea cture
                        </Text>
                        </Card>
                    </View>
                </TouchableOpacity>
            )
        }
    }

    renderRestaurant() {
        console.log(this.state.restaurants)
        return (
            <FlatList
                horizontal={true}
                data={this.state.restaurants}
                renderItem={({ item }) => this.renderRestaurantCard(item)}
                keyExtractor={item => item.restaurant.id.toString()}
            />
        )
    }

    render() {
        return (
            <View>
                {this.renderRestaurant()}
            </View>
        );
    }
}

const styles = {
    restaurantCard: {
        height: 250,
        width: 250,
        borderRadius: 5
    },
    title: {
        fontSize: 15,
        color: 'white',
        width: 400
    }
}