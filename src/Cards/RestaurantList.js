import React, { Component } from 'react';
import { Text, Image, View, StyleSheet } from 'react-native';
import { Card, CardItem, Body, Left } from 'native-base';

/**
 * This component will display Restaurant List
 * Following props this will receive
 * 
 * imageUri - Thumbnail of Restaurant
 * name - name of the Restaurant
 * rating - rating of the Restaurant
 * locality - locality of the Restaurant
 * cuisines- types of cuisines
 * */
const RestaurantList = (props) => {
    if (props.imageUri != "") {
        return (
            <Card style={styles.cardContainer}>
                <CardItem >
                    <Left>
                        <Image source={{ uri: props.imageUri }}
                            style={{ height: 100, width: 100 }}
                        />
                        <Body >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.title}>{props.name}</Text>
                                <Text style={styles.rating} >
                                    {props.rating}
                                </Text>
                            </View>
                            <Text note>{props.locality}</Text>
                            <Text note>{props.cuisines}</Text>
                        </Body>
                    </Left>
                </CardItem>
            </Card>
        );
    } else {
        return (
            <Card style={styles.cardContainer}>
                <CardItem >
                    <Left>
                        <Image source={require('../assests/thumb.jpg')}
                            style={styles.headerImage}
                        />
                        <Body >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.title}>{props.name}</Text>
                                <Text style={styles.rating} >
                                    {props.rating}
                                </Text>
                            </View>
                            <Text note>{props.locality}</Text>
                            <Text note>{props.cuisines}</Text>
                        </Body>
                    </Left>
                </CardItem>
            </Card>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rating: {
        backgroundColor: 'green',
        color: 'white',
        padding: 4,
        flex: 1
    },
    cardContainer: {
        marginLeft: 5,
        marginRight: 5
    },
    title: {
        fontSize: 15,
        color: 'black',
        flex: 9
    },
    header: {
        backgroundColor: 'white'
    },
    headerImage: { 
        height: 100,
        width: 100 
    }

});

export default RestaurantList;