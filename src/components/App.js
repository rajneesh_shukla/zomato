import React, { Component } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  Image
} from 'react-native';
import { createDrawerNavigator, DrawerItems, createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';

import HomeScreen from '../screens/HomeScreen';
import NearbyRestaurantScreen from '../screens/NearbyRestaurantScreen';
import { configureStore } from '../redux/configureStore';
import SearchScreen from '../screens/SearchScreen';
import RestaurantDetail from '../screens/RestaurantDetailScreen';

export default class App extends Component {
  render() {
    const store = configureStore();
    return (
      <Provider store={store}>
        <AppDrawerNavigator />
      </Provider>
    );
  }
}

// Custom Drawer
const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={styles.drawerHeader}>
      <Image
        style={styles.headerImage}
        source={require('../assests/restaurant.png')} />
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)


// Home Navigator 
const HomeNavigator = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
  },
  SearchScreen: {
    screen: SearchScreen
  },
  RestaurantDetail: {
    screen: RestaurantDetail
  },
  NearbyRestaurantScreen: {
    screen: NearbyRestaurantScreen
  }
},
  {
    initialRouteName: 'HomeScreen'
  })


//Restaurants Navigator
const RestaurantsNavigator = createStackNavigator({
  NearbyRestaurantScreen: {
    screen: NearbyRestaurantScreen
  },
  RestaurantDetail: {
    screen: RestaurantDetail
  }
},
  {
    initialRouteName: 'NearbyRestaurantScreen'
  })


// Drawer Navigator
const AppDrawerNavigator = createDrawerNavigator({
  Home: HomeNavigator,
  Restaurant: RestaurantsNavigator,
},
  {
    contentComponent: CustomDrawerComponent
  }
)



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  drawerHeader: { 
    height: 150,
    backgroundColor: 'white',
    justifyContent: 'center', 
    alignContent: 'center' 
  },
  headerImage: { 
    height: 120, 
    width: 120, 
    margin: 30
  }
});
