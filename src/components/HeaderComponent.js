import React from 'react';
import { Header, Left, Icon} from 'native-base';
import {View} from 'react-native';

/**
 *  Display Header on Top 
 */
const HeaderComponent = (props) => {
    const {navigate} = this.props.navigation;
    return(
        <View>
            <Header style={{backgroundColor: 'white'}} navigation={navigate}>
                <Left>
                    <Icon
                        onPress={ ()=> props.navigation.openDrawer()}
                        name="menu" />
                </Left>
            
            </Header>
        </View>
    );
}

export default HeaderComponent;