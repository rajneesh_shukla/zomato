import React, { Component } from 'react';
import {
     Text,
     View, 
     Image,
     PermissionsAndroid, 
     TouchableOpacity, 
     ActivityIndicator,
     ToastAndroid } from 'react-native';
import { Card, CardItem, Left, Right } from 'native-base';
import { SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchNearbyRestaurants } from '../redux/actions/ActionFetchRestaurants';

import RestaurantComponent from '../components/RestaurentCompoent';
import { RestaurantCard } from '../Cards/RestaurantCard';

/**
 *  Display Home screen Location and Restaurants Cards
 */
class LocationComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            location: 'Searching...',
            lastLat: 0,
            lastLng: 0,
            restaurants: null,
            locationDetails: null
        }
    }


    componentWillMount() {
        this.fetchLocation();
        this.requestLocationPermission();
    }

    componentWillReceiveProps(nextProp) {
        //set data is receive
        this.setState({
            restaurants: nextProp.Restaurants.restaurants.nearby_restaurants,
            location: nextProp.Restaurants.restaurants.location.title,
            locationDetails: nextProp.Restaurants.restaurants
        })
    }

    componentDidMount() {
        this.setState({
            restaurants: this.props.Restaurants.restaurants.nearby_restaurants,
            locationDetails: this.props.Restaurants.restaurants
        })
    }


    // Request Location permission
    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('location permission granted');
                this.fetchLocation();
            } else {
                console.log('location permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }


    // Set the state with current location coordinate
    fetchLocation = () => {
        ToastAndroid.show('Fetching current location...', ToastAndroid.SHORT);
      
        navigator.geolocation.watchPosition(position => {
         
            // Create the object to update this.state.mapRegion through the onRegionChange function
            const region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
            };
            
            console.log(region);

            this.setState({
                lastLat: region.latitude,
                lastLng: region.longitude
            })
            // fetch the restaurant data bases on lat lng
            this.props.fetchNearbyRestaurants(region.latitude, region.longitude);
            console.log(this.state)
        });
    }

    //Display Nearby Restaurants
    showRestaurants() {
        if (this.state.restaurants != null) {
            // Return Restaurant data in a card
            return <RestaurantCard restaurantItem={this.state.restaurants} navigation={this.props.navigation} />
        } else {
            return (
                <View style={styles.indicator}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }
    }

    render() {
        return (
            <View>
                <Card style={styles.cardContainer}>

                    <CardItem>
                        <Left >
                            <Image style={styles.locationIcon}
                            source={require('../assests/location_icon.png') } />
                            <Text style={{ alignContent: 'flex-start' }}>{this.state.location}</Text>
                        </Left>
                        <Right>
                            <TouchableOpacity onPress={() => this.fetchLocation()} >
                                <Text style={{ color: 'green' }}>
                                    change
                                </Text>
                            </TouchableOpacity>
                        </Right>
                    </CardItem>

                    <CardItem >
                        <View onPress={() => console.log("clicked")}>
                            <SearchBar
                                containerStyle={{ backgroundColor: 'white' }}
                                round={true}
                                style={styles.searchBarStyle}
                                lightTheme
                                icon={{ type: 'font-awesome', name: 'search' }}
                                placeholder='Search for restaurant, cuisines or dish...'
                                onChangeText={() => this.props.navigation.navigate('SearchScreen',
                                                  { restaurantData: this.state.locationDetails })}
                                onPress={() => this.props.navigation.navigate('SearchScreen')}
                            />
                        </View>
                    </CardItem>
                </Card>

                <Card style={styles.cardContainer} transparent >
                    <RestaurantComponent restaurants={this.state.restaurants} navigation={this.props.navigation} />
                    {this.showRestaurants()}
                </Card>

            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("Location Component called ")
    return {
        Restaurants: state.nearbyRestaurantReducer
    }
}

const styles = {
    cardContainer: {
       
    },
    searchBarStyle: {
        backgroundColor: 'while',
        width: '100%'
    },
    indicator: { 
        justifyContent: 'center', 
        alignContent: 'center', 
        flex: 1 
    },
    locationIcon: {
        height: 15, 
        width: 15
    }
}

export default connect(mapStateToProps, { fetchNearbyRestaurants })(LocationComponent);
