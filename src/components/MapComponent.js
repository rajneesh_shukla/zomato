import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';

/**
 * Display Nearby Restaurants on Map
 */
class MapComponent extends Component {

  constructor(props) {
    super(props); 
    console.log(this.props.restaurants)
    super(props);
  }

  componentWillMount() {
    console.log(this.props.restaurants.location.latitude)
    console.log(parseFloat(this.props.restaurants.location.longitude))
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
            latitude: parseFloat(this.props.restaurants.location.latitude),
            longitude: parseFloat(this.props.restaurants.location.longitude),
            latitudeDelta: 0.0105,
            longitudeDelta: 0.0101,
          }}
        >

          {this.props.restaurants.nearby_restaurants.map(marker => (
            <Marker
              coordinate={{ latitude: parseFloat(marker.restaurant.location.latitude), 
                            longitude: parseFloat(marker.restaurant.location.longitude)}}
              title={marker.restaurant.name}
              description={marker.restaurant.user_rating.aggregate_rating}
            />
          ))}

        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 500,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
export default MapComponent;