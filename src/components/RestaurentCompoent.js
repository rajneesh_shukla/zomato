import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Left, Right, Card, CardItem } from 'native-base';
import { createStackNavigator } from 'react-navigation';
import NearbyRestaurantScreen from '../screens/NearbyRestaurantScreen';

/**
 * Display Restaurant Header Information 
 */

class RestaurentComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            restaurant: this.props.restaurants
        }
    }

    showRestaurant() {
        console.log("Restaurant");
        console.log(this.state);
        return (
            <Text>Hello</Text>
        )
    }

    render() {
        return (
            <View>
                <View style={styles.container}>
                    <Text style={styles.recemendText}>Recemendations for you</Text>
                </View>

                <Card transparent>
                    <CardItem>
                        <Left>
                            <Text style={{ color: 'black', fontSize: 15 }}>Go out of lunch or dinner</Text>
                        </Left>
                        <Right>
                            <Text style={{ color: 'green' }}
                                onPress={() => this.props.navigation.navigate('NearbyRestaurantScreen')}
                            >see more</Text>
                        </Right>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const RestaurantListNavigator = createStackNavigator({
    NearbyRestaurantScreen: NearbyRestaurantScreen
})

styles = {
    container: {
        border: 1
    },
    recemendText: {
        fontSize: 20,
        color: 'black',
        marginLeft: 20,
        marginRight: 20,
        borderBottomColor: 'black',
        borderBottomWidth: 0.2,
        marginTop: 10,
        paddingBottom: 7
    }
}

export default RestaurentComponent;